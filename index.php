<?php
require __DIR__ .'/config.php';
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>淘金股管理端</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/main.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="bower_components/kindeditor/themes/default/default.css" />

  <script>
    window.MainServer = '<?=MainServer?>';
    window.ChatAppKey = '<?=ChatAppKey?>';
    window.PCUrl = '<?=PCUrl?>';
    window.internalReferenceId = '<?=internalReferenceId?>';
    window.researchReportId = '<?=researchReportId?>';
  </script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <script src="dist/js/jquery.cookie.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script src="bower_components/vue/vue.js"></script>
  <script src="bower_components/vue/vue-resource.js"></script>
  <!-- plugins -->
  <script src="dist/js/spin.js"></script>
  <script src="plugins/ckeditor/ckeditor.js"></script>
  <!-- common -->
  <script src="dist/js/common.js"></script>
  <script src="dist/js/service.js"></script>
  <?php require __DIR__.'/layout/init.htm';?>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="bower_components/raphael/raphael.min.js"></script>
  <script src="bower_components/morris.js/morris.min.js"></script>
  <!-- Sparkline -->
  <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <script src="dist/js/vue-html5-editor.js"></script>
  <script charset="utf-8" src="bower_components/kindeditor/kindeditor-all.js"></script>
  <script charset="utf-8" src="bower_components/kindeditor/lang/zh-CN.js"></script>
  <script src="https://cdn.bootcss.com/babel-polyfill/7.0.0-beta.44/polyfill.min.js"></script>
  <script src="dist/js/aliyun.oss.upload.js"></script>
  <script src="dist/js/clipboard.js"></script>
  <script src="dist/js/vue-plugins.js"></script>
  <script src="plugins/layer/layer.js"></script>
  <!-- 引入样式 -->
  <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-chalk/index.css">
  <!-- 引入组件库 -->
  <script src="https://unpkg.com/element-ui/lib/index.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- Google Font -->
<!--  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">-->

<!--word图片上传-->
<link type="text/css" rel="Stylesheet" href="plugins/WordPaster/css/WordPaster.css" />
<link type="text/css" rel="Stylesheet" href="plugins/WordPaster/js/skygqbox.css" />
<script type="text/javascript" src="plugins/WordPaster/js/json2.min.js" charset="utf-8"></script>
<script type="text/javascript" src="plugins/WordPaster/js/w.edge.js" charset="utf-8"></script>
<script type="text/javascript" src="plugins/WordPaster/js/w.app.js" charset="utf-8"></script>
<script type="text/javascript" src="plugins/WordPaster/js/w.file.js" charset="utf-8"></script>
<script type="text/javascript" src="plugins/WordPaster/js/WordPaster.js" charset="utf-8"></script>
<script type="text/javascript" src="plugins/WordPaster/js/skygqbox.js" charset="utf-8"></script>

    <link href="https://cdn.bootcss.com/element-ui/1.4.6/theme-default/index.css" rel="stylesheet">
    <!--<script src="https://cdn.bootcss.com/vue/2.4.4/vue.js"></script>-->
    <script src="https://cdn.bootcss.com/element-ui/1.4.6/index.js"></script>
    <!--<script src="https://cdn.bootcss.com/axios/0.16.2/axios.min.js"></script>-->

</head>
<body class="hold-transition skin-blue sidebar-mini fixed">

<?php
// 通用组件
require __DIR__.'/components/file-uploader.htm';
require __DIR__.'/components/pagination.htm';
require __DIR__.'/components/page.htm';
require __DIR__.'/components/date-picker.htm';
require __DIR__.'/components/kind-editor.htm';
require __DIR__.'/components/ck-editor.htm';
require __DIR__.'/components/video-uploader.htm';
require __DIR__.'/components/position-modal.htm';


?>
<div id="vue-components" style="display:none"></div>


<div class="wrapper">

  <?php require __DIR__.'/layout/main-header.htm'?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php require __DIR__.'/layout/left-side.htm'?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="content-wrapper-parent">
      <div id="alerts-container" style="display: none; position: fixed;">
          <div>
              <div class="alert success alert-success alert-dismissible" >
                  <strong>操作成功！<span class="msg-text"></span></strong>
              </div>
              <div class="alert info alert-info alert-dismissible" >
                  <strong>提示：<span class="msg-text"></span></strong>
              </div>
              <div class="alert warning alert-warning alert-dismissible" >
                  <strong>请注意：<span class="msg-text"></span></strong>
              </div>
              <div class="alert error alert-danger alert-dismissible" >
                  <strong>操作失败！<span class="msg-text"></span></strong>
              </div>
          </div>
      </div>
  </div>
  <!-- /.content-wrapper -->

    <div class="waiting-for-box" style="display: none">
        <div class="waiting-for"><div id="spin-element"></div></div>
    </div>

  <?php //require __DIR__.'/layout/main-footer.htm'?>

  <?php //require __DIR__.'/layout/control-sitebar.htm'?>


</div>
<!-- ./wrapper -->

<script>
    (function () {
        var opts = {
            lines: 13, // The number of lines to draw
            length: 5, // The length of each line
            width: 4, // The line thickness
            radius: 16, // The radius of the inner circle
            corners: 0.6, // Corner roundness (0..1)
            rotate: 0, // The rotation offset
            color: '#000', // #rgb or #rrggbb
            speed: 2, // Rounds per second
            trail: 32, // Afterglow percentage
            shadow: false, // Whether to render a shadow
            hwaccel: false, // Whether to use hardware acceleration
            className: 'spinner', // The CSS class to assign to the spinner
            zIndex: 2e9, // The z-index (defaults to 2000000000)
            top: 'auto', // Top position relative to parent in px
            left: 'auto' // Left position relative to parent in px
        };
        var spinner = new Spinner(opts).spin(document.getElementById('spin-element'));
    }());

    (function () {
        /**
         * 检查登录
         */
        try{
            var a = getGlobalAccount();
            if (!a) window.location.hash = '#login';
        }catch (e){
            window.location.hash = '#login';
        }
    })();
  $(function () {

    function switchPage(page_id, target) {
      var id = page_id.substr(1);
      $("#content-wrapper-parent>div").each(function (i, a) {
        if (a.id == id) $(a).fadeIn();
        else $(a).hide();
      });
      return;
      $("#sidebar-menu>li>a").each(function (i, a) {
        var h = a.href;
        var item = $(a).parent();
//        var menu = item.parent().parent();

        if (h.substr(h.indexOf('#') + 1) == target) {
            item.addClass('active');
//            menu.addClass('menu-open');
        }
        else {
            item.removeClass('active');
//            menu.removeClass('menu-open');
        }
      });
    }

    function route() {

      var hash = window.location.hash;
      setTimeout(function () {
          left_site_app.setMenuActive(hash);
      }, 500);
      if (!hash) return ;

      var target = hash.substr(1);
      var page_uri = 'page/' + target + '.htm?time='+(new Date().getTime());
      var page_id = "#page-async-"+target;

      var parent = $("#content-wrapper-parent");
      var page = parent.children(page_id);
      if (page.length == 0) {
        $.get(page_uri, function (resp) {
          parent.append('<div class="page-async-container" id="'+page_id.substr(1)+'" >'+resp+'</div>');
          switchPage(page_id, target);
        });
      } else {
        switchPage(page_id, target);
      }

      window.scroll(0,0);
    }

    window.addEventListener('hashchange', route);

    if (window.location.hash.length == 0) window.location.hash = '#index';
    else route();
  });

</script>
</body>
</html>
