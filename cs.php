<?php
require __DIR__ . '/config.php';
?>
<!DOCTYPE html>
<html ng-app="demo" class="ng-scope">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
    h3{font-family: "Microsoft Yahei", Arial, sans-serif}
    [ng\:cloak], [ng-cloak], [data-ng-cloak], [x-ng-cloak], .ng-cloak, .x-ng-cloak,
    .ng-hide:not(.ng-hide-animate) {display: none !important;}
    ng\:form {display: block;}
    .ng-animate-shim {visibility: hidden;}
    .ng-anchor {position: absolute;}
    .conversation-box{min-height: 500px;}
    </style>
    <title>客服端</title>
    <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="chat/rong_lib/jquery-rebox-0.1.0.css">
    <link rel="stylesheet" type="text/css" href="chat/rong_lib/RongIMWidget.css">
    <link rel="stylesheet" type="text/css" href="chat/rong_lib/main.css">

    <script src="chat/rong_lib/protobuf-2.1.6.min.js"></script>
    <style type="text/css">.RC_Expression {
        width: 24px;
        height: 24px;
        background-image: url(//cdn.ronghub.com/css-sprite_bg-2.1.10.png);
        display: inline-block
    }</style>
    <script>
        window.MainServer = '<?=MainServer?>';
        window.ChatAppKey = '<?=ChatAppKey?>';
    </script>
    <?php require __DIR__ . '/layout/init.htm' ?>
</head>
<body ng-controller="main" class="ng-scope">


<div class="container-fluid">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        </div>
    </div>

    <div class="row" ng-show="showDiv.login">
        <div class="col-md-2 col-md-offset-4">
            <div class="login-box">
                <div class="form">
                    <h3>客服登录</h3>
                    <div class="form-group">
                        <label>帐号：</label>
                        <input class="form-control" ng-model="serviceAccount.account">
                    </div>
                    <div class="form-group">
                        <label>密码：</label>
                        <input class="form-control" ng-model="serviceAccount.password" type="password">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary" ng-click="serviceLogin()">登录</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" ng-show="showDiv.conversation">
        <div class="col-md-6 col-md-offset-3">
            <h3><strong ng-bind="service.name"></strong></h3>
            <div class="conversation-box">
                <h3><span class="ng-binding" ng-bind="serviceName"></span></h3><br>
                <rong-widget></rong-widget>
            </div>
        </div>
    </div>

</div>



<script src="chat/rong_lib/jquery-2.2.2.js"></script>
<script src="dist/js/jquery.cookie.js"></script>
<script src="chat/rong_lib/angular-1.4.8.js"></script>

<!-- 融云IMLib -->
<script src="chat/rong_lib/RongIMLib-2.2.5.min.js"></script>
<script src="chat/rong_lib/RongEmoji-2.2.5.min.js"></script>
<script src="chat/rong_lib/Libamr-2.2.5.min.js"></script>
<script src="chat/rong_lib/RongIMVoice-2.2.5.min.js"></script>

<!-- 上传插件 -->
<script src="chat/rong_lib/plupload.full.min-2.1.1.js"></script>
<script src="chat/rong_lib/qiniu-1.0.17.js"></script>

<!-- 增强体验插件 -->
<script src="chat/rong_lib/jquery-rebox-0.1.0.js"></script>

<!-- IM插件 -->
<script src="chat/rong_lib/RongIMWidget.js"></script>
<script src="chat/rong_lib/common.js"></script>
<script src="chat/rong_lib/index.js"></script>

<script src="dist/js/common.js"></script>
<script src="chat/rong_lib/navi.js"></script>
</body>
</html>