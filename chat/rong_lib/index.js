var demo = angular.module("demo", ["RongWebIMWidget"]);

demo.controller("main", ["$scope", "WebIMWidget", "$http", function ($scope, WebIMWidget, $http) {

    $scope.serviceAccount = {
        account: '',
        password: ''
    };

    $scope.service = {};

    $scope.serviceLogin = function () {

        ajax({
            uri: '/account/customer_service_login',
            data: $scope.serviceAccount,
            success: function (resp) {
                $scope.serviceAccount = {
                    account: '',
                    password: ''
                };
                $scope.service = {
                    id: resp.chat.id,
                    name: resp.chat.name,
                    avatar: resp.chat.avatar
                };
                $scope.switchShow('conversation');
                let config = {
                    appkey: window.ChatAppKey,
                    token: resp.chat.token,
                    displayConversationList: true,
                    style: {
                        left: 3,
                        bottom: 3,
                        width: 430
                    },
                    onSuccess: function (id) {
                        $scope.user = id;
                        console.log('连接成功：', id);
                    },
                    onError: function (error) {
                        console.log('连接失败：', error);
                    }
                };
                RongDemo.common(WebIMWidget, config, $scope);
            }
        });

    };

    $scope.showDiv = {
        login: true,
        conversation: false
    };
    $scope.switchShow = function (p) {
        for (let prop in $scope.showDiv) {
            $scope.showDiv[prop] = (prop == p);
        }
    };



}]);