(function () {
/*
    将相同代码拆出来方便维护
 */
window.RongDemo = {
    common: function (WebIMWidget, config, $scope) {
        WebIMWidget.init(config);

        WebIMWidget.setUserInfoProvider(function(targetId, obj) {
            obj.onSuccess({
                name: "用户：" + targetId
            });
        });

        $scope.show = function() {
            WebIMWidget.show();
        };

        $scope.hidden = function() {
            WebIMWidget.hidden();
        };

        WebIMWidget.show();

        // 示例：获取 userinfo.json 中数据，根据 targetId 获取对应用户信息
        WebIMWidget.setUserInfoProvider(function(targetId,obj){
            ajax({
                uri: '/account/get_chat_user_info',
                data: {id: targetId},
                success: function (resp) {
                    let user = resp.data;
                    obj.onSuccess({id: user.id, name: user.name, portraitUri: user.avatar});
                }
            });
        //     $http({
        //       url:"/userinfo.json"
        //     }).success(function(rep){
        //
        //       var user = rep.data;
        //
        //       if(user){
        //         obj.onSuccess({id:user.id,name:user.name,portraitUri:user.portraitUri});
        //       }else{
        //         obj.onSuccess({id:targetId,name:"用户："+targetId});
        //       }
        //     });
        });

        // 示例：获取 online.json 中数据，根据传入用户 id 数组获取对应在线状态
        // WebIMWidget.setOnlineStatusProvider(function(arr, obj) {
        //     ajax({
        //         uri: '/account/check_chat_user_online',
        //         data: {id: arr[0]},
        //         success: function (resp) {
        //             obj.onsuccess(resp.data.online);
        //         }
        //     })
        // });
    }
};

})();