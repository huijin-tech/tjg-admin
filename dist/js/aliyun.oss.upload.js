/**
 * 上传阿里云方法，功能：在底层操作，不包括视图和DOM的操作
 * 
 * @author 客串一回
 */

function aliOSSUpload(file, filetype, success, progress, error) {


    let uploadSuccess = function (file, resp, success, progress) {
		if (200 !== resp.code) throw new DOMException(resp);

		let data = resp.data;

        let t = Date.now(), name = file.name;
        let path = data.dir + '/' + t + randomString() + name.substr(name.lastIndexOf('.'));

        // 创建表单
        let form = new FormData();
		form.append('policy', data.policy);
		form.append('OSSAccessKeyId', data.accessid);
		form.append('success_action_status', 200);
		form.append('signature', data.signature);
		form.append('key', path);
		form.append('file', file);

		// js原生请求
		let xhr = new XMLHttpRequest();
		if (typeof progress === "function") {
            xhr.upload.addEventListener('progress', progress, false);
		}
		if (typeof error === "function") {
			xhr.addEventListener('error', error);
		}
		xhr.onreadystatechange = function () {
			if (4 === xhr.readyState) {
				if (200 === xhr.status) {
					// 成功
					let resp = xhr.responseText;
					console.log(resp);
					if (typeof success === "function") {
                        success((data.dns || data.host) + '/' + path);
					}
				}
			}
        };
		xhr.open('POST', data.host);
		xhr.send(form);

	};

	// 获取policy后上传
	ajax({
		uri: '/alioss/get_policy',
		data: {type: filetype},
		success: function (resp) {
			uploadSuccess(file, resp, success, progress);
		},
		error: function (err) {
			console.log(err);
        }
	});

}
