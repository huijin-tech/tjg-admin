/**
 * Created by jjj on 2017/8/14.
 */



function showAlert(status, msg, expired = 1000) {
    var p = $("#alerts-container");
    p.find(".alert").hide();
    var a = p.find(".alert."+status);
    a.show();
    a.find('.msg-text').text(msg ? msg : '');
    p.fadeIn(300);
    setTimeout(function () {
        p.fadeOut(200);
    }, expired);
}

function toggleWaiting(show) {
    if (show) $(".waiting-for-box").show();
    else $(".waiting-for-box").hide();
}

function adminUrl(path) {
    if (path[0] !== '/') path = '/' + path;
    return MainServer + '/admin' + path;
}

function adminHeaders() {
    var headers = {
        appid:3,
        appsecret:'335a42771860bfb06dceb49f531f8de7',
    };

    var admin = getGlobalAccount();
    if (admin) {
        headers.uid = admin.id;
        headers.token = admin.token;
    }

    return headers;
}

var AJAX_CONST = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE'
}

function ajax(options) {

    if (!options.uri) throw new DOMException('uri不能为空');

    if (!options.type) {
        options.type = 'POST';
    }
    toggleWaiting(true);
    $.ajax({
        url: adminUrl(options.uri),
        type: (options.type || AJAX_CONST.GET),
        headers: adminHeaders(),
        data: options.data,
        success: function (resp) {
            toggleWaiting(false);
            switch (resp.code) {
                case 200:
                    showAlert('success');
                    options.success(resp);
                    break;
                case 1011: // 登录提示
                    logout();
                    break;
                case 1010:
                    showAlert('error', resp.msg, 3000);
                    break;
                case 1021: // 没有权限
                case 400:
                case 401:
                case 404:
                case 500:
                default:
                    if (typeof options.error === "function") {
                        options.error(resp);
                    } else {
                        console.log(options.uri, options.data);
                        showAlert('error', resp.msg);
                        throw new DOMException(JSON.stringify(resp));
                    }
                    break;
            }
        },
        error: function (err) {
            console.log(err);
            toggleWaiting(false);
            showAlert('error', '请求异常');
        }
    });

}

function downloadFile(uri, params) {
    var headers = adminHeaders();
    var url = adminUrl(uri)+'?uid='+headers.uid+'&token='+headers.token;
    for (var p in params) {
        if (null === params[p]) continue;
        url += '&'+p+'='+params[p];
    }
    console.log(url);
    window.open(url);
}

function getTimestamp(time) {
    if (time instanceof Date) {
        return parseInt(time.getTime() / 1000);
    } else {
        return parseInt(new Date(time).getTime() / 1000);
    }
}


function getDateFromTimestamp(timestamp) {
    return new Date(timestamp * 1000).toLocaleDateString();
}


function formatDate(date) {
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();

    return y+"-"+(m<10?"0"+m:m)+"-"+(d<10?"0"+d:d);
}


function randomString(len) {
    len = len || 32;
    var $chars = 'ABCDEFGHIJKMNOPQRSTUVWXYZabcdefhijklmnoprstuvwxyz123456789'; /****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
    var maxPos = $chars.length;
    var pwd = '';
    for (i = 0; i < len; i++) {
        pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}


function parseUrl(url) {

    var r = {
        protocol: /([^\/]+:)\/\/(.*)/i,
        host: /(^[^\:\/]+)((?:\/|:|$)?.*)/,
        port: /\:?([^\/]*)(\/?.*)/,
        pathname: /([^\?#]+)(\??[^#]*)(#?.*)/
    };

    var tmp, res = {};
    res["href"] = url;
    for (p in r) {
        tmp = r[p].exec(url);
        res[p] = tmp[1];
        url = tmp[2];
        if (url === "") {
            url = "/";
        }
        if (p === "pathname") {
            res["pathname"] = tmp[1];
            res["search"] = tmp[2];
            res["hash"] = tmp[3];
        }
    }

    return res;
}


(function () {
    Date.prototype.format = function(fmt = 'yyyy-mm-dd hh:ii:ss')
    { //author: meizz
        var o = {
            "m+" : this.getMonth()+1,                 //月份
            "d+" : this.getDate(),                    //日
            "h+" : this.getHours(),                   //小时
            "i+" : this.getMinutes(),                 //分
            "s+" : this.getSeconds(),                 //秒
            "q+" : Math.floor((this.getMonth()+3)/3), //季度
            "S"  : this.getMilliseconds()             //毫秒
        };
        if(/(y+)/.test(fmt))
            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length));
        for(var k in o)
            if(new RegExp("("+ k +")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
        return fmt;
    }

})();
