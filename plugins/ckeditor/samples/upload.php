<?php
/**
 * Created by IntelliJ IDEA.
 * User: jjj
 * Date: 2018/3/28
 * Time: 17:59
 */

error_log(json_encode($_FILES,JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE));

$path = 'upload/' . time() . mt_rand(10000,99999) . '.jpg';
if (is_uploaded_file($_FILES['upload']['tmp_name'])) {
	move_uploaded_file($_FILES['upload']['tmp_name'], __DIR__ .'/'. $path);
}

header('content-type:application/json');
echo json_encode([
	'url' => $path,
	'uploaded' => true,
]);
